import AmsomTextarea from '../AmsomTextarea.vue';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
export default {
  title: 'AmsomTextarea',
  component: AmsomTextarea,
  tags: ['autodocs'],
  argTypes: {
    //backgroundColor: {
    //  control: 'color',
    //},
    //onClick: {},
    //size: {
    //  control: {
    //    type: 'select',
    //  },
    //  options: ['small', 'medium', 'large'],
    //},
  },
};

// More on writing stories with args: https://storybook.js.org/docs/writing-stories/args
export const Default = {
  args: {
  },
};

export const NoMessage = {
  args: {
    maxlength: 20
  },
};
